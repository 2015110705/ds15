/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef int iType;
typedef struct {
	int key;
	iType item;
	}element;
typedef struct node *treePointer;
typedef struct node {
	element		data;
	treePointer leftChild;
	treePointer rightChild;
	}tNode;

element* search(treePointer root, int key);
element* iterSearch(treePointer tree, int k);
treePointer modifiedSearch(treePointer tree, int key);
void insert(treePointer *node, int k, iType theItem);
void inorder(treePointer node);

int main()
{
	int			n;	/* 이진 탐색트리의 노드 개수 */
	int			seed; /* 시드 값 */
	treePointer root			= NULL;
	element*	temp			= NULL;
	int			randomNumber;
	int			inputKey;
	int			i;
	
	/* 이진 탐색트리의 노드 개수 입력 받기 */
	printf("random number generation <1 ~ 500> \n");
	printf("the number of nodes in BST <less than and equal to 50> : ");
	scanf("%d", &n);

	/* seed 받기 */
	printf("seed : ");
	scanf("%d", &seed);
	srand(seed);

	/* create BST */
	printf("\ncreating a BST from random numbers\n");
	for(i = 0; i < n; i++) {
		/* get random number (0 ~ 500) */
		randomNumber = rand() % 500 + 1;
		
		/* check duplication */
		while (search(root, randomNumber) != NULL) {
			randomNumber = rand() % 500 + 1;
		}

		/* insert to tree */
		insert(&root, randomNumber, (iType)randomNumber);
	}

	/* input key value */
	printf("the key to search : ");
	scanf("%d", &inputKey);

	/* binary search and print result */
	temp = search(root, inputKey); 
	if (temp != NULL) {
		printf("the element's item is %d \n", temp->item);
	}
	else {
		printf("there is no such element \n\n");	
	}

	/* inorder traversal */
	printf("inorder traversal of the BST shows the sorted sequence \n");
	inorder(root);
	printf("\n");

	return 0;
}

/**
 * Recursive search of a binary search tree
 * @param	: root
 * @param	: key for searching
 */
element* search(treePointer root, int key)
{
	/*	return a pointer to the element whose key is k, if
		there is no such element, return NULL. */
	if (!root) {
		return NULL;
	}
	if (key == root->data.key) {
		return &(root->data);
	}
	if (key < root->data.key) {
		return search(root->leftChild, key);
	}
	return search(root->rightChild, key);
}

/**
 * Iterative serach of a binary search tree
 * @param	: root of tree
 * @param	: key for searching
 */
element* iterSearch(treePointer tree, int k)
{
	/*	return a pointer to the element whose key is k, if
		there is no such element, return NULL. */
	while (tree) {
		if (k == tree->data.key) {
			return &(tree->data);
		}
		if (k < tree->data.key) {
			tree = tree->leftChild;
		}
		else {
			tree = tree->rightChild;
		}
	}
	return NULL;
}

treePointer modifiedSearch(treePointer tree, int key)
{
	treePointer temp;

	/* if BST is empty then return NULL */
	if (tree == NULL) {
		return NULL;
	}

	/* find key value */
	while(tree) {
		/* if there is key, then return NULL*/
		if (key == tree->data.key) {
			return NULL;
		}

		temp = tree;
		/* or not, continue the traversal */
		if (key < tree->data.key) {
			tree = tree->leftChild;
		}
		else {
			tree = tree->rightChild;
		}
	}

	/* last node pointer */
	return temp;
}

/**
 * Inserting a dictinoary pair into a binary search tree
 * @param	: root
 * @param	: k of element
 * @param	: theItem of element
 */
void insert(treePointer *node, int k, iType theItem)
{
	/*	if k is in the tree pointed at by node do nothing;
		otherwise add a new node with data - (k, theItem) */
	treePointer ptr = NULL; 
	treePointer	temp = modifiedSearch(*node, k);

	if (temp || !(*node)) {
		/* k is not in the tree */
		ptr = (treePointer)malloc(sizeof(tNode));
		ptr->data.key = k;
		ptr->data.item = theItem;
		ptr->leftChild = ptr->rightChild = NULL;
		if (*node) {
			/* insert as child of temp */	
			if (k < temp->data.key) {
				temp->leftChild = ptr;
			}
			else {
				temp->rightChild = ptr;
			}
		}
		else {
			*node = ptr;
		}
	}
}

/**
 * inorder traversal of a binary tree
 * @param	: root of tree
 */
void inorder(treePointer node)
{
	if (node) {
		inorder(node->leftChild);
		printf("%3d ", node->data.key);
		inorder(node->rightChild);
	}
}