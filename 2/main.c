/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAX_SIZE 100
#define INF_NUM 10000

/* min - winner tree */
int nums[MAX_SIZE + 1] = { 0 };			//keys to sort	
int winTree[2 * MAX_SIZE] = { NULL };	//winner tree
int sorted[MAX_SIZE + 1] = { 0 };		//sorted result

int initWinner(int cur, int k, int winTree[]);
void adjustWinner(int min, int k, int winTree[]);
void inorder(int root, int k, int winTree[]);

int main()
{
	int seed;	//seed value
	int n;		//number of random value (n is power of 2)
	int i;

	printf("<<<<<<<<<<<<<< sorting with winner tree >>>>>>>>>>>>>>>>>> \n\n");
	
	/* input seed and k  */
	printf("the number of keys <8, 16, or 32 as a power of 2 >\t>> ");
	scanf("%d", &n);
	printf("random numbers to use as key values <1 ~ 100> \n");
	printf("seed >> ");
	scanf("%d", &seed);
	srand(seed);

	/* save random numbers to nums (1 ~ 100 and duplicate available)*/
	for (i = 1; i <= n; i++) {
		nums[i] = rand() % 100 + 1;
		printf("%3d ",nums[i]);
		winTree[n + i - 1] = i;
	}
	printf("\n\n");

	/* initialize winTree */
	printf("initialization of min-winner tree \n\n");
	initWinner(1, n, winTree);

	/* inorder traversal for min-winner tree */
	printf("\ninorder traversal for min-winner tree \n");
	inorder(1, n, winTree);

	/* sorting with min-winner tree */
	printf("\nsorting with min-winner tree.. \n");
	adjustWinner(winTree[1], n, winTree);

	/* print sort result */
	printf("sorted result : \n");
	for (i = 1; i <= n; i++) {
		printf("%3d ", sorted[i]);
	}
	printf("\n");

	return 0;
}

/**
 * initialize winTree
 * format : recursive postorder traversal
 * @param	: cur for recursive
 * @param	: k is number of random value
 * @param	: winTree
 */
int initWinner(int cur, int k, int winTree[])
{
	/*	winner tree is complete binary tree and
		save sequentially according to node level 
		each node of winner tree has only pointer(or index) of key value(or record) */
	if (cur < k * 2) {
		//inorder
		initWinner(cur * 2, k, winTree);
		initWinner(cur * 2 + 1, k, winTree);

		if (cur % 2 == 1) {
			if (nums[winTree[cur - 1]] >= nums[winTree[cur]]) {
				winTree[cur / 2] = winTree[cur];
			}
			else {
				winTree[cur / 2] = winTree[cur - 1];
			}
		}
	}
}

void adjustWinner(int min, int k, int winTree[])
{
	int i;

	for (i = 1; i <= k; i++) {
		sorted[i] = nums[winTree[1]];
		nums[winTree[1]] = 999;
		initWinner(1, k, winTree);
	}
}

void inorder(int root, int k, int winTree[])
{
	if (root < k * 2) {
		inorder(root * 2, k, winTree);
		printf("%2d ", nums[winTree[root]]);
		inorder(root * 2 + 1, k, winTree);
	}
}
